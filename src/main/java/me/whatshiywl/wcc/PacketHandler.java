package me.whatshiywl.wcc;

import net.minecraft.network.INetworkManager;
import net.minecraft.network.packet.Packet250CustomPayload;
import cpw.mods.fml.common.network.IPacketHandler;
import cpw.mods.fml.common.network.Player;

public class PacketHandler implements IPacketHandler {

	@Override
	public void onPacketData(INetworkManager manager, Packet250CustomPayload packet, Player entityPlayer) {
		if (packet.channel.equals("WesterosCapes")) {
			String[] args = new String(packet.data).split(";");
			if(args[0].equals("ADD")){ //ADD;playername;id
				String id = args[2];
				WesterosCapesClient.capeUsers.put(args[1], id);
			} else if(args[0].equals("REMOVE")){ //Remove caped player
				WesterosCapesClient.capeUsers.remove(args[1]);
			} else { //id;url
				String id = args[0];
				String url = args[1];
				ThreadDownloadCapesData tdcd = new ThreadDownloadCapesData(id, url);
				CapeManager.setCape(id, tdcd);
				tdcd.setDaemon(true);
				tdcd.setName("WesterosCapes cape info downloader: " + id);
				tdcd.start();
			}
		}
	}
}