package me.whatshiywl.wcc;

import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.client.renderer.IImageBuffer;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.StringUtils;
import me.whatshiywl.wcc.proxy.CommonProxy;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPostInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;

@Mod(modid="WesterosCapesClient", name="WesterosCapes Client", version="@VERSION@")
@NetworkMod(clientSideRequired=false, serverSideRequired=false, channels={"WesterosCapes"}, packetHandler = PacketHandler.class)
public class WesterosCapesClient{

	@SidedProxy(clientSide="me.whatshiywl.wcc.proxy.ClientProxy", serverSide="me.whatshiywl.wcc.proxy.CommonProxy")
	public static CommonProxy proxy;
	public static HashMap<String, String> capeUsers = new HashMap<String, String>();

	@Mod.EventHandler
	public static void preInit(FMLPreInitializationEvent event) {}

	@Mod.EventHandler
	public static void init(FMLInitializationEvent event){
		proxy.initTick();
	}

	@Mod.EventHandler
	public static void postInit(FMLPostInitializationEvent event) {}

	public static ResourceLocation capeStaticResourceLocation(String username){
		return new ResourceLocation("westeroscapes/" + StringUtils.stripControlCodes(username));
	}

	public static ResourceLocation capeAnimatedResourceLocation(String username, int frameNumber){
		return new ResourceLocation("westeroscapes/" + StringUtils.stripControlCodes(username) + "/" + frameNumber);
	}

	public static ThreadDownloadImageData downloadCapeImage(ResourceLocation rl, String url){
		Method getDownloadImage = null;
		try {
			Class<?>[] parameters = {ResourceLocation.class, String.class, ResourceLocation.class, IImageBuffer.class};
			getDownloadImage = AbstractClientPlayer.class.getDeclaredMethod("func_110301_a", parameters);
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		if(getDownloadImage.getReturnType().equals(ThreadDownloadImageData.class)){
			getDownloadImage.setAccessible(true);
		}
		try {
			return (ThreadDownloadImageData) getDownloadImage.invoke(AbstractClientPlayer.class, rl, url, (ResourceLocation)null, (IImageBuffer)null);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		return null;
	}
}
