package me.whatshiywl.wcc;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.client.entity.AbstractClientPlayer;
import net.minecraft.util.ResourceLocation;
import cpw.mods.fml.common.ITickHandler;
import cpw.mods.fml.common.TickType;

import java.lang.reflect.Field;
import java.util.EnumSet;

public class TickHandler implements ITickHandler {

	private static final Minecraft mc = Minecraft.getMinecraft();
	private Field downloadImageCape, locationCape;
	private int tickCnt = 0;

	public TickHandler(){
		Class<AbstractClientPlayer> cls = AbstractClientPlayer.class;
		try{
			downloadImageCape = cls.getDeclaredField("field_110315_c");
			if(downloadImageCape.getType().isAssignableFrom(ThreadDownloadImageData.class)){
				downloadImageCape.setAccessible(true);
			}
			locationCape = cls.getDeclaredField("field_110313_e");
			if(locationCape.getType().isAssignableFrom(ResourceLocation.class)){
				locationCape.setAccessible(true);
			}
		} catch (NoSuchFieldException e){
			e.printStackTrace();
		}
	}

	public void tickStart(EnumSet<TickType> type, Object... tickData){
	    this.tickCnt++;
	    // Only check for cape changes once/second - kind of overkill to do every tick
	    if (this.tickCnt < 20) {
	        return;
	    }
	    this.tickCnt = 0;
	    
		if(mc.theWorld != null) {
		    for (Object p : mc.theWorld.playerEntities) {
                AbstractClientPlayer player = (AbstractClientPlayer) p;
				String id = WesterosCapesClient.capeUsers.get(player.username);
				if(id == null){
					continue;
				}
				ThreadDownloadCapesData tdcd = CapeManager.getCape(id);
				if(tdcd == null) continue;
				if(tdcd.finishedLoading){
					try{
						downloadImageCape.set(player, tdcd.getCurrentCapeImage());
						locationCape.set(player, tdcd.getCurrentCapeRL());
					} catch (IllegalArgumentException e){
						e.printStackTrace();
					}
					catch (IllegalAccessException e){
						e.printStackTrace();
					}
					catch (NullPointerException e){
						e.printStackTrace();
					}
				}
			}
		}
	}

	public void tickEnd(EnumSet<TickType> type, Object... tickData){}

	public EnumSet<TickType> ticks(){
		return EnumSet.of(TickType.CLIENT);
	}

	public String getLabel(){
		return "WesterosCapesClientTickHandler";
	}
}
