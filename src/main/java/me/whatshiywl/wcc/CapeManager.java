package me.whatshiywl.wcc;

import java.util.HashMap;

public class CapeManager {
	public static HashMap<String, ThreadDownloadCapesData> capes = new HashMap<String, ThreadDownloadCapesData>();

	public static ThreadDownloadCapesData getCape(String id){
		return capes.get(id);
	}

	public static void setCape(String id, ThreadDownloadCapesData tdcd){
		capes.put(id, tdcd);
	}
}
