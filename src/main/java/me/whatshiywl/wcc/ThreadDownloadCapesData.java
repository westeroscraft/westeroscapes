package me.whatshiywl.wcc;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.ThreadDownloadImageData;
import net.minecraft.util.ResourceLocation;

import java.util.ArrayList;

public class ThreadDownloadCapesData extends Thread {

	public String id;
	public String url;
	public boolean staticCapeEnabled = false;
	public boolean animatedCapeEnabled = false;
	public String staticCapeUrl;
	public ArrayList<String> animatedCapeFrameUrls;
	public ResourceLocation staticCapeRL;
	public ResourceLocation[] animatedCapeFramesRL;
	public ThreadDownloadImageData staticCapeImage;
	public ThreadDownloadImageData[] animatedCapeFramesImages;
	public int currentCapeFrame;
	public long lastCapeFrameTime;
	public int capeAnimationInterval = 500;
	boolean finishedLoading = false;

	public ThreadDownloadCapesData(String username, String url){
		this.id = username;
		this.url = url;
	}

	public void run(){
		try{

			if(id.startsWith("S")){
				this.staticCapeEnabled = true;
				this.staticCapeUrl = url;
			}
			else if(id.startsWith("D")){
				this.animatedCapeEnabled = true;
				if(this.animatedCapeFrameUrls == null){
					this.animatedCapeFrameUrls = new ArrayList<String>();
				}
				this.animatedCapeFrameUrls.add(url);
			}
			else if(url.startsWith("capeAnimationInterval:")){
				//TODO: this
				this.capeAnimationInterval = (Integer.parseInt(url) * 10);
			}
			downloadImageData();
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}

	private void downloadImageData(){
		if(this.animatedCapeEnabled){
			this.animatedCapeFramesRL = new ResourceLocation[this.animatedCapeFrameUrls.size()];
			for (int i = 0; i < this.animatedCapeFrameUrls.size(); i++){
				this.animatedCapeFramesRL[i] = WesterosCapesClient.capeAnimatedResourceLocation(this.id, i + 1);
			}
			this.animatedCapeFramesImages = new ThreadDownloadImageData[this.animatedCapeFrameUrls.size()];
			for (int i = 0; i < this.animatedCapeFrameUrls.size(); i++){
				this.animatedCapeFramesImages[i] = WesterosCapesClient.downloadCapeImage(this.animatedCapeFramesRL[i], (String)this.animatedCapeFrameUrls.get(i));
			}
		}
		else if(this.staticCapeEnabled){
			//this.staticCapeRL = WesterosCapesClient.capeStaticResourceLocation(this.username);
			this.staticCapeRL = WesterosCapesClient.capeStaticResourceLocation(id);
			this.staticCapeImage = WesterosCapesClient.downloadCapeImage(this.staticCapeRL, this.staticCapeUrl);
		}
		this.finishedLoading = true;
	}

	public ThreadDownloadImageData getCurrentCapeImage(){
		if(this.animatedCapeEnabled){
			long time = Minecraft.getSystemTime();
			if(time > this.lastCapeFrameTime + this.capeAnimationInterval){
				this.lastCapeFrameTime = time;
				this.currentCapeFrame += 1;
				if(this.currentCapeFrame >= this.animatedCapeFramesImages.length){
					this.currentCapeFrame = 0;
				}
			}
			return this.animatedCapeFramesImages[this.currentCapeFrame];
		}
		return this.staticCapeImage;
	}

	public ResourceLocation getCurrentCapeRL(){
		if(this.animatedCapeEnabled){
			return this.animatedCapeFramesRL[this.currentCapeFrame];
		}
		return this.staticCapeRL;
	}
}
